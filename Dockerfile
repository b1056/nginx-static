FROM ubuntu
RUN apt-get update && \
  apt install -y nginx libnginx-mod-http-cache-purge
COPY . /var/www/html
EXPOSE 80
CMD nginx -g 'daemon off;'